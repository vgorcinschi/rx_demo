package com.workjam.demo.util;

import java.util.Optional;
import java.util.function.Function;

public class MessageUtils {

    public static <T> Function<Object, Optional<T>> castToGenericOptional() {
        return obj -> (Optional<T>) obj;
    }
}