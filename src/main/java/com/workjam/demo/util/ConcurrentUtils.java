package com.workjam.demo.util;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

public class ConcurrentUtils {

    public static <T> CompletableFuture<Stream<T>> streamOfFuturesToFutureStream(Stream<CompletableFuture<T>> toConvert) {
        //first transform stream to array
        CompletableFuture<T>[] futures = toConvert.toArray(CompletableFuture[]::new);
        //when all futures complete, join their values into one stream
        return CompletableFuture.allOf(futures)
                .thenApply(v -> Arrays.stream(futures).map(CompletableFuture::join));
    }
}
