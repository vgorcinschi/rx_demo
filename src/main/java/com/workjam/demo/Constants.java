package com.workjam.demo;

final class Constants {
    static String HOST = "http.host";
    static String PORT = "http.port";
    static String WORKJAM_DEMO_SYSTEM = "workjamAkkaDemo";
    static String REQUEST_TIMEOUT = "akka.http.server.request-timeout";
}
