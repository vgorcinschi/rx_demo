package com.workjam.demo.unmarshaller;

import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.model.HttpEntity;
import akka.http.javadsl.unmarshalling.Unmarshaller;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventDescription {
    public final static Unmarshaller<HttpEntity, EventDescription> UNMARSHALLER = Jackson.unmarshaller(EventDescription.class);

    private int nrOfTickets;
}