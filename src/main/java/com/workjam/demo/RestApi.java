package com.workjam.demo;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.http.javadsl.marshallers.jackson.Jackson;
import akka.http.javadsl.server.AllDirectives;
import akka.http.javadsl.server.PathMatchers;
import akka.http.javadsl.server.Route;
import akka.util.Timeout;
import com.workjam.demo.TicketSeller.Tickets;
import com.workjam.demo.messages.boxoffice.*;
import com.workjam.demo.unmarshaller.EventDescription;
import scala.concurrent.ExecutionContextExecutor;

import java.util.Optional;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import static akka.http.javadsl.model.StatusCodes.*;
import static akka.http.javadsl.server.PathMatchers.segment;
import static akka.pattern.PatternsCS.ask;
import static com.workjam.demo.util.MessageUtils.castToGenericOptional;
import static io.vavr.API.*;
import static io.vavr.Predicates.instanceOf;

/**
 * Class defines routes for our Http server
 */
public class RestApi extends AllDirectives {

    private ActorSystem system;
    private Timeout requestTimeout;
    private ExecutionContextExecutor ec;

    //our top level actor
    private ActorRef boxOffice;

    public RestApi(ActorSystem system, Timeout requestTimeout) {
        this.system = system;
        this.requestTimeout = requestTimeout;
        this.ec = system.dispatcher();
        this.boxOffice = system.actorOf(BoxOffice.props(requestTimeout), BoxOffice.NAME);
    }

    //#all-routes
    public Route routes() {
        return route(eventsRoute(), eventRoute(), ticketsRoute());
    }

    //#get-events - for "/events/"
    private Route eventsRoute() {
        return pathPrefix("events",
                () -> pathEndOrSingleSlash(
                        () -> get(() -> onSuccess(this::getEvents, events -> complete(OK, events, Jackson.marshaller())))
                )
        );
    }

    //for "/events/:event"
    private Route eventRoute() {
        return pathPrefix(PathMatchers.segment("events").slash(segment()),
                event -> pathEndOrSingleSlash(() -> route(
                        post(() -> entity(EventDescription.UNMARSHALLER, //unmarshalling body
                                eventDescription -> onSuccess(() -> createEvent(event, eventDescription.getNrOfTickets()), eventPostRoute(event)))),
                        get(() -> onSuccess(() -> getEvent(event), eventGetRoute())),
                        delete(() -> onSuccess(() -> cancelEvent(event), eventGetRoute()))//resolution is same as above
                )));
    }

    //#get-tickets - for "events/:event/tickets"
    private Route ticketsRoute() {
        return pathPrefix(PathMatchers.segment("events").slash(segment()).slash("tickets"),
                event -> pathEndOrSingleSlash(() -> route(
                        post(() -> entity(EventDescription.UNMARSHALLER, //unmarshalling body
                                request -> onSuccess(() -> requestTickets(event, request.getNrOfTickets()), ticketsPostRoute())))
                )));
    }


    private Function<Optional<Event>, Route> eventGetRoute() {
        return maybeEvent -> maybeEvent.map(event -> (Route) complete(OK, event, Jackson.marshaller())).orElseGet(() -> complete(NOT_FOUND));
    }

    private Function<EventResponse, Route> eventPostRoute(String event) {
        return eventResponse -> Match(eventResponse).of(
                Case($(instanceOf(EventExists.class)), complete(BAD_REQUEST, String.format("Event %s exists already", event))),
                Case($(instanceOf(EventCreated.class)), complete(CREATED, ((EventCreated) eventResponse).getEvent(), Jackson.marshaller())));
    }

    private Function<Tickets, Route> ticketsPostRoute() {
        return ticketsResponse -> {
            if (ticketsResponse.getEntries().isEmpty()) return complete(NOT_FOUND);
            return complete(CREATED, ticketsResponse, Jackson.marshaller());
        };
    }

    private CompletionStage<EventResponse> createEvent(String event, int nrOfTickets) {
        return ask(boxOffice, new CreateEvent(event, nrOfTickets), requestTimeout).thenApply(EventResponse.class::cast);
    }

    private CompletionStage<Events> getEvents() {
        return ask(boxOffice, new GetEvents(), requestTimeout).thenApply(Events.class::cast);
    }

    private CompletionStage<Optional<Event>> getEvent(String event) {
        return ask(boxOffice, new GetEvent(event), requestTimeout).thenApply(castToGenericOptional());
    }

    private CompletionStage<Optional<Event>> cancelEvent(String event) {
        return ask(boxOffice, new CancelEvent(event), requestTimeout).thenApply(castToGenericOptional());
    }

    private CompletionStage<Tickets> requestTickets(String event, int nrOfTickets) {
        return ask(boxOffice, new GetTickets(event, nrOfTickets), requestTimeout).thenApply(Tickets.class::cast);
    }
}