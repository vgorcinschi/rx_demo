package com.workjam.demo.messages.boxoffice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//case classes
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateEvent {
    private String name;
    private int tickets;
}
