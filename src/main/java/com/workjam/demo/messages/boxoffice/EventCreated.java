package com.workjam.demo.messages.boxoffice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventCreated implements EventResponse {
    private Event event;
}
