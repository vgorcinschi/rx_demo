package com.workjam.demo;

import akka.actor.AbstractActor;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import com.google.common.collect.Lists;
import com.workjam.demo.messages.boxoffice.Event;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.pcollections.PVector;
import org.pcollections.TreePVector;

import java.util.List;
import java.util.Optional;

public class TicketSeller extends AbstractActor {

    private LoggingAdapter log = Logging.getLogger(getContext().getSystem(), this);
    private PVector<Ticket> tickets = TreePVector.empty();
    private String event;

    private TicketSeller(String event) {
        this.event = event;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                //add tickets -> add all new to existing tickets
                .match(Add.class, add -> tickets = tickets.plusAll(add.getTickets()))
                //buy n tickets
                .match(Buy.class, this::receiveMsgBuy)
                .match(GetEvent.class, this::receiveMsgGetEvent)
                .match(Cancel.class, cancel -> receiveMsgCancel())
                .build();
    }

    private void receiveMsgGetEvent(GetEvent getEvent) {
        log.info("Get event received for {}", event);
        getSender().tell(Optional.of(new Event(event, tickets.size())), getSelf());
    }

    private void receiveMsgCancel() {
        getSender().tell(Optional.of(new Event(event, tickets.size())), getSelf());
        getSelf().tell(PoisonPill.getInstance(), getSelf());
    }

    /**
     * If nr. of requested tickets is bigger then {@link this#tickets}
     * then we will return 0 tickets, else we will return
     * the requested nr. of tickets and adjust {@link this#tickets} accordingly
     * @param buy message sent from supervisor
     */
    private void receiveMsgBuy(Buy buy) {
        int nrOfTickets = buy.getTickets();
        if(tickets.size() >= nrOfTickets) {
            log.info("Selling {} tickets for {} event", nrOfTickets, event);
            PVector<Ticket> entries = tickets.subList(0, nrOfTickets);
            tickets = tickets.minusAll(entries);
            getSender().tell(new Tickets(event, entries), getSelf());
        } else
            getSender().tell(new Tickets(event), getSelf());
    }

    static Props props(String event) {
        return Props.create(TicketSeller.class, () -> new TicketSeller(event));
    }


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Add {
        private List<Ticket> tickets;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Buy {
        private int tickets;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ticket {
        private int id;
    }

    @Data
    @AllArgsConstructor
    public static class Tickets {
        private String event;
        private List<Ticket> entries = Lists.newArrayList();

        public Tickets(String event) {
            this.event = event;
        }
    }

    public static class GetEvent {}
    public static class Cancel {}
}
