package com.workjam.demo;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.util.Timeout;
import com.workjam.demo.TicketSeller.Ticket;
import com.workjam.demo.messages.boxoffice.*;
import com.workjam.demo.util.ConcurrentUtils;
import com.workjam.demo.util.OptionalConsumer;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static akka.pattern.PatternsCS.ask;
import static akka.pattern.PatternsCS.pipe;
import static java.util.stream.Collectors.toList;

public class BoxOffice extends AbstractActor {

    private Timeout timeout;
    final static String NAME = "boxOffice";

    //create child actors
    private ActorRef createTicketSeller(String name) {
        return getContext().actorOf(TicketSeller.props(name), name);
    }

    private BoxOffice(Timeout timeout) {
        this.timeout = timeout;
    }

    //the only method of an actor
    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(CreateEvent.class, this::receiveMsgCreateEvent)
                .match(GetTickets.class, this::receiveMsgGetTickets)
                .match(GetEvent.class, this::receiveMsgGetEvent)
                .match(GetEvents.class, this::receiveMsgGetEvents)
                .match(CancelEvent.class, this::receiveMsgCancelEvent)
                .build();
    }

    private void receiveMsgCreateEvent(CreateEvent createEvent) {
        Optional<ActorRef> maybeChild = getChildByName(createEvent.getName());
        OptionalConsumer.of(maybeChild)
                .ifPresent(child -> getSender().tell(new EventExists(), getSelf()))
                .ifNotPresent(() -> create(createEvent.getName(), createEvent.getTickets()));
    }

    private void receiveMsgGetTickets(GetTickets getTickets) {
        Optional<ActorRef> maybeChild = getChildByName(getTickets.getEvent());
        OptionalConsumer.of(maybeChild)
                .ifPresent(child -> child.forward(new TicketSeller.Buy(getTickets.getTickets()), getContext()))
                .ifNotPresent(() -> getSender().tell(new TicketSeller.Tickets(getTickets.getEvent()), getSelf()));
    }

    private void receiveMsgGetEvent(GetEvent getEvent) {
        Optional<ActorRef> maybeChild = getChildByName(getEvent.getName());
        OptionalConsumer.of(maybeChild)
                .ifPresent(child -> child.forward(new TicketSeller.GetEvent(), getContext()))
                .ifNotPresent(() -> getSender().tell(Optional.empty(), getSelf()));
    }

    private void receiveMsgGetEvents(GetEvents getEvents) {
        //ask self() for each of the passed-in event
        Stream<CompletableFuture<Optional<Event>>> listFutureMaybeEvent =
                allChildrenStream()
                .map(child ->
                        ask(getSelf(), new GetEvent(child.path().name()), timeout)
                        .thenApply(obj -> (Optional<Event>) obj)
                        .toCompletableFuture());

        pipe(futureEvents(listFutureMaybeEvent).thenApply(Events::new), getContext().dispatcher()).to(sender());
    }

    private CompletableFuture<List<Event>> futureEvents(Stream<CompletableFuture<Optional<Event>>> listFutureMaybeEvent) {
        return ConcurrentUtils.streamOfFuturesToFutureStream(listFutureMaybeEvent)
                .thenApply(optionalStream -> optionalStream
                        .filter(Optional::isPresent)
                        .map(Optional::get)
                        .collect(toList()));
    }

    private Stream<ActorRef> allChildrenStream() {
        return StreamSupport.stream(getContext().getChildren().spliterator(), false);
    }

    private void receiveMsgCancelEvent(CancelEvent cancelEvent) {
        Optional<ActorRef> maybeChild = getChildByName(cancelEvent.getName());
        OptionalConsumer.of(maybeChild)
                .ifPresent(child -> child.forward(new TicketSeller.Cancel(), getContext()))
                .ifNotPresent(() -> getSender().tell(Optional.empty(), getSelf()));
    }

    private void create(String name, int nrOfTickets) {
        ActorRef newTicketSeller = createTicketSeller(name);
        List<Ticket> tickets = IntStream.range(1, nrOfTickets + 1).mapToObj(Ticket::new).collect(toList());
        //add new nrOfTickets to ticket seller
        newTicketSeller.tell(new TicketSeller.Add(tickets), getSelf());
        //confirm to sender that the event is created
        getSender().tell(new EventCreated(new Event(name, nrOfTickets)), getSelf());
    }

    private Optional<ActorRef> getChildByName(String name) {
        return getContext().findChild(name);
    }

    //Props is a configuration class to specify options for the creation of actors
    static Props props(Timeout timeout) {
        return Props.create(BoxOffice.class, () -> new BoxOffice(timeout));
    }
}