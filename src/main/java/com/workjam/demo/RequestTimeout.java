package com.workjam.demo;

import akka.util.Timeout;
import com.typesafe.config.Config;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

import static com.workjam.demo.Constants.REQUEST_TIMEOUT;

public interface RequestTimeout {

    default Timeout requestTimeout(Config config){
        String to = config.getString(REQUEST_TIMEOUT);
        Duration d = Duration.create(to);
        return Timeout.durationToTimeout(FiniteDuration.create(d.length(), d.unit()));
    }
}
