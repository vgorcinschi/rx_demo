package com.workjam.demo;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.http.javadsl.ConnectHttp;
import akka.http.javadsl.Http;
import akka.http.javadsl.ServerBinding;
import akka.http.javadsl.model.HttpRequest;
import akka.http.javadsl.model.HttpResponse;
import akka.stream.ActorMaterializer;
import akka.stream.javadsl.Flow;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.io.IOException;
import java.util.concurrent.CompletionStage;

import static com.workjam.demo.Constants.*;

public class Application implements RequestTimeout{

    public static void main(String[] args) throws IOException {
        new Application().run();
    }

    private void run() throws IOException {
        Config config = ConfigFactory.load();
        final String host = config.getString(HOST);
        final int port = Integer.parseInt(config.getString(PORT));

        //All actors are hierarchical(incl for Fault Tolerance). We need one root for our hierarchy.
        ActorSystem system = ActorSystem.create(WORKJAM_DEMO_SYSTEM);
        final Http http = Http.get(system);
        final ActorMaterializer actorMaterializer = ActorMaterializer.create(system); //needed to run flows

        final RestApi restApi = new RestApi(system, requestTimeout(config));
        final Flow<HttpRequest, HttpResponse, NotUsed> flow = restApi.routes().flow(system, actorMaterializer);
        //starts new Http Server using the "flow" handler
        CompletionStage<ServerBinding> binding = http.bindAndHandle(flow, ConnectHttp.toHost(host, port), actorMaterializer);

        System.out.println(String.format("Server online at %s:%d\nPress return to stop...", host, port));
        System.in.read(); // let it run until user presses return

        binding.thenCompose(ServerBinding::unbind).thenAccept(unbound -> system.terminate());
    }
}